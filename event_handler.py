import socket
from concurrent_queue import ConcurrentQueue
from file_info_model import FileList
from global_variable import *
from transfer_manager import TransferClient, TransferClientPool
from thread_alive_status import ThreadAliveStatus


class NotifyEventHandler:
    def __init__(self, target_ip, current_local_file_event_queue: ConcurrentQueue,
                 current_unrequested_queue: ConcurrentQueue, file_list: FileList):
        self.target_ip = target_ip

        self.file_list = file_list
        # Link queue below to File Monitor
        self.current_local_file_event_queue = current_local_file_event_queue

        # Link queue below to address file send and receive (Event Looper)
        self.current_unrequested_queue = current_unrequested_queue

        # Transfer Client
        self.transfer_client = TransferClient(self.target_ip, self.file_list)
        self.transfer_client_dispatcher = TransferClientPool(self.transfer_client)

    def monitor_queue_and_send_to_remote(self, socket_handler: socket, is_sub_thread_alive: ThreadAliveStatus):
        while is_sub_thread_alive.is_sub_thread_alive:
            if not self.current_unrequested_queue.is_empty():
                file_path = self.current_unrequested_queue.dequeue()
                file_path_encoded = (file_path).encode('utf-8')
                # print(f"Request File From Remote{file_path}")
                socket_handler.sendall(OP_REQUEST_FILE)
                socket_handler.sendall(len(file_path_encoded).to_bytes(4, byteorder='big'))
                socket_handler.sendall(file_path_encoded)
                print(f"Request File From Remote{file_path} sent")


            # on_local_notify_remote_file_to_request
            elif not self.current_local_file_event_queue.is_empty():
                file_event = self.current_local_file_event_queue.dequeue()
                event_type = file_event.event_type
                file_path_encoded = (file_event.file_path).encode('utf-8')
                # print(f"Send event to remote{file_event.file_path}")
                socket_handler.sendall(event_type)
                socket_handler.sendall(len(file_path_encoded).to_bytes(4, byteorder='big'))
                socket_handler.sendall(file_path_encoded)
                print(f"File event {file_event.file_path} sent")

    def receive_notification_from_remote(self, socket_handler: socket, is_sub_thread_alive: ThreadAliveStatus):
        while is_sub_thread_alive.is_sub_thread_alive:
            opcode = socket_handler.recv(1)
            if opcode == OP_NOTIFY_FILE_ADDED or opcode == OP_NOTIFY_FILE_CHANGED:
                path_length = int.from_bytes(socket_handler.recv(4), byteorder='big')
                file_path = socket_handler.recv(path_length).decode('utf-8')
                self.current_unrequested_queue.enqueue(file_path)


            elif opcode == OP_REQUEST_FILE:
                path_length = int.from_bytes(socket_handler.recv(4), byteorder='big')
                file_path = socket_handler.recv(path_length).decode('utf-8')
                print("[Notification Service] File requested by remote: " + file_path)
                # Start new file transfer client thread to send file
                self.transfer_client_dispatcher.submit(file_path)


            elif len(opcode) == 0:
                print("[Notification Service] Connection Lost, Exiting...")
                is_sub_thread_alive.is_sub_thread_alive = False
                self.transfer_client_dispatcher.clear_task_pool()
                break
