class ThreadAliveStatus:
    def __init__(self, is_sub_thread_alive: bool):
        self.is_sub_thread_alive: bool = is_sub_thread_alive
