import hashlib


def get_file_md5(filename):
    md5_hashlib = hashlib.sha1()
    with open(filename, 'rb') as f:
        for chunk in iter(lambda: f.read(65536), b''):
            md5_hashlib.update(chunk)
    return md5_hashlib.hexdigest()
