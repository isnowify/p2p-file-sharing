import argparse
import os
from threading import Thread
from file_info_model import FileList
from concurrent_queue import ConcurrentQueue
from socket_manager import SocketManager
from file_monitor import FileMonitor


def _init_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip', action='store', required=True,
                        dest='ip', help='The ip of the desired machine that establish connection on')
    return parser.parse_args()


def _start_file_monitor(file_monitor: FileMonitor):
    file_monitor.start_monitoring()


def _init_notification_client(socket_manager: SocketManager):
    # socket_manager.run_notification_service()
    socket_manager.run_notification_client()


def _init_notification_server(socket_manager: SocketManager):
    # socket_manager.run_notification_service()
    socket_manager.run_notification_server()


def _init_transfer_server(socket_manager: SocketManager):
    socket_manager.run_transfer_server()


if __name__ == '__main__':
    arg_parser = _init_parser()
    target_folder = './share'
    current_local_file_event_queue = ConcurrentQueue()
    target_ip = arg_parser.ip

    # Create share folder if not exist
    if not os.path.exists(target_folder):
        os.mkdir(target_folder)

    # Init file list first
    file_list = FileList()
    initial_file_list = file_list.init_file_list(target_folder)

    # Init File Monitor Service
    monitor = FileMonitor(target_folder, current_local_file_event_queue, file_list)
    file_monitor_thread = Thread(target=_start_file_monitor, args=(monitor,), name='FileMonitor')
    file_monitor_thread.start()

    # Init File Notification Service
    socket_manager = SocketManager(target_ip, initial_file_list, current_local_file_event_queue, file_list)

    notification_client_thread = Thread(target=_init_notification_client, args=(socket_manager,), name='NotifyService')
    notification_client_thread.start()
    notification_server_thread = Thread(target=_init_notification_server, args=(socket_manager,), name='NotifyService')
    notification_server_thread.start()

    # Init File Transfer Server
    notification_socket_thread = Thread(target=_init_transfer_server, args=(socket_manager,), name='TransferServer')
    notification_socket_thread.start()
