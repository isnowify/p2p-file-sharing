from socket import *
from threading import Thread

from global_variable import *
import file_info_model
from exception_manager import *
from concurrent_queue import ConcurrentQueue
from event_handler import NotifyEventHandler
from thread_alive_status import ThreadAliveStatus


class NotifyServer:
    def __init__(self, target_ip, initial_file_list, file_to_request, file_to_send,
                 current_unrequested_queue: ConcurrentQueue, notify_event_handler: NotifyEventHandler):
        self.initial_file_list = initial_file_list
        self.target_ip = target_ip
        self.is_running = False
        self.clients_connected_socket = {}
        self.is_file_list_diff_finished = False
        self.file_list_diff_result = []
        self.file_to_request = file_to_request
        self.file_to_send = file_to_send
        self.current_unrequested_queue = current_unrequested_queue
        self.notify_event_handler = notify_event_handler
        self.is_sub_thread_alive = ThreadAliveStatus(True)

    def restore_status(self, initial_file_list):
        self.initial_file_list = initial_file_list

    def send_notification(self, socket_handler: socket):
        self.notify_event_handler.monitor_queue_and_send_to_remote(socket_handler, self.is_sub_thread_alive)
        # print("Sending Queue Stopped")

    def receive_notification(self, socket_handler: socket):
        self.notify_event_handler.receive_notification_from_remote(socket_handler, self.is_sub_thread_alive)
        # print("Receiving Queue stopped")

    def __client_handler(self, server_socket: socket, address):
        # print(f"[Notification Server] {self.target_ip}: Connected to {address}")

        # Initial Handshake with client
        handshake_success_flag = False
        try:
            pre_handshake_code = server_socket.recv(1)
            if len(pre_handshake_code) == 0:
                raise FileSharingException('Handshake Error')
            elif pre_handshake_code == OP_HANDSHAKE:
                print(f"[Notification Server] {self.target_ip}: Handshake with {address} success")
                handshake_success_flag = True
                server_socket.sendall(OP_HANDSHAKE)
            else:
                raise FileSharingException('Handshake Error')
        except Exception as e:
            print(f"[Notification Server] Handshake with client error {e.__str__()}, will close this session.")
            server_socket.close()

        # Start to retrieve for file list from client
        pre_handshake_code = server_socket.recv(1)
        if len(pre_handshake_code) == 0:
            raise FileSharingException('File List Get Error')
        elif pre_handshake_code == OP_SEND_FILE_LIST:
            json_length = int.from_bytes(server_socket.recv(4), byteorder='big')
            file_table_byte = server_socket.recv(json_length)
            while len(file_table_byte) < json_length:
                file_table_byte += server_socket.recv(json_length - len(file_table_byte))
            # print(f"[Notification Server] Received File Table from Client:   {file_table_byte}")
            other_file_list = file_info_model.file_list_deserialize(file_table_byte)
            self.file_to_request, self.file_to_send = file_info_model.get_file_list_diff(
                this_file_list=self.initial_file_list,
                other_file_list=other_file_list)
            # print(f"Initial File List: {self.initial_file_list}   id : {id(self.initial_file_list)}")
            # print(f"current_unrequested_queue if {id(self.current_unrequested_queue)}")
            # print("[Notification Server] File list diff created, sending to client...")
            # print("[Notification Server] Client's File list diff (Remote): ", self.file_to_send)
            # print("[Notification Server] Server's File list diff (This Device): ", self.file_to_request)
        else:
            raise FileSharingException('File List Get Error')

        # Store files in current_unrequested_queue
        if not len(self.file_to_request) == 0:
            for files_unrequested in self.file_to_request:
                self.current_unrequested_queue.enqueue(files_unrequested)

        # Send file list diff to client
        server_socket.sendall(OP_SEND_FILE_LIST_DIFF)
        file_list_diff_byte = file_info_model.file_list_serialize(self.file_to_send)
        print("[Notification Server] Start transferring file diff list.")
        server_socket.sendall(len(file_list_diff_byte).to_bytes(4, byteorder='big'))
        server_socket.sendall(file_list_diff_byte)
        print("[Notification Server] File diff list sent.")

        # Init notification service
        notification_receive_thread = Thread(target=self.receive_notification, args=(server_socket,),
                                             name="NotificationSend")
        notification_receive_thread.start()

        notification_send_thread = Thread(target=self.send_notification,
                                          args=(server_socket,), name="NotificationReceive")
        notification_send_thread.start()

    def init_server_listen(self):
        print(f"[Notification Server] {self.target_ip}: Start Listening on Port {NOTIFY_PORT}")
        server_socket_handler = socket(AF_INET, SOCK_STREAM)
        server_socket_handler.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        server_socket_handler.bind(("", NOTIFY_PORT))
        server_socket_handler.listen(50)
        self.is_running = True
        while True:
            server_socket, address = server_socket_handler.accept()
            self.clients_connected_socket[address] = server_socket
            client_handler_thread = Thread(target=self.__client_handler, args=(server_socket, address,))
            client_handler_thread.start()


class NotifyClient:
    def __init__(self, target_ip, initial_file_list, file_to_request,
                 current_unrequested_queue: ConcurrentQueue, notify_event_handler: NotifyEventHandler,
                 restore_status_handler):
        self.initial_file_list = initial_file_list
        self.target_ip = target_ip
        self.is_running = False
        self.file_to_request = file_to_request
        self.current_unrequested_queue = current_unrequested_queue
        self.notify_event_handler = notify_event_handler
        self.is_sub_thread_alive = ThreadAliveStatus(True)
        self.restore_status_handler = restore_status_handler

    def send_notification(self, socket_handler: socket):
        self.notify_event_handler.monitor_queue_and_send_to_remote(socket_handler, self.is_sub_thread_alive)

    def receive_notification(self, socket_handler: socket, ):
        self.notify_event_handler.receive_notification_from_remote(socket_handler, self.is_sub_thread_alive)

    def init_client_connection(self):
        try:
            print(f"[Notification Client] Start connecting to {self.target_ip}")
            client_socket = socket(AF_INET, SOCK_STREAM)
            client_socket.connect((self.target_ip, NOTIFY_PORT))
            print(f"[Notification Client] Successfully connected to {self.target_ip}")
            self.is_running = True

            # Initial Handshake with client
            handshake_success_flag = False
            client_socket.sendall(OP_HANDSHAKE)
            try:
                pre_handshake_code = client_socket.recv(1)
                if len(pre_handshake_code) == 0:
                    raise FileSharingException('Handshake Error')
                elif pre_handshake_code == OP_HANDSHAKE:
                    print(f"[Notification Client] {self.target_ip}: Handshake with {self.target_ip} success")
                    handshake_success_flag = True
                else:
                    raise FileSharingException('Handshake Error')
            except Exception as e:
                print(f"[Notification Client] Handshake with client error {e.__str__()}, will close this session.")
                client_socket.close()

            if not handshake_success_flag:
                client_socket.close()
            else:
                client_socket.sendall(OP_SEND_FILE_LIST)
                client_file_list_byte = file_info_model.file_list_serialize(self.initial_file_list)
                print("[Notification Client] Start transferring file list.")
                client_socket.sendall(len(client_file_list_byte).to_bytes(4, byteorder='big'))
                client_socket.sendall(client_file_list_byte)

                # Receive file list diff from server
                if (client_socket.recv(1) == OP_SEND_FILE_LIST_DIFF):
                    # print("[Notification Client] Start receiving file list diff.")
                    file_list_diff_byte_len = int.from_bytes(client_socket.recv(4), byteorder='big')
                    file_list_diff_byte = client_socket.recv(file_list_diff_byte_len)
                    while len(file_list_diff_byte) < file_list_diff_byte_len:
                        file_list_diff_byte += client_socket.recv(file_list_diff_byte_len - len(file_list_diff_byte))
                    self.file_to_request = file_info_model.file_list_deserialize(file_list_diff_byte)
                    # print("[Notification Client] Finish receiving file list diff.")
                # print(f"[Notification Client]  File to request: {self.file_to_request}")

                # Store files in current_unrequested_queue
                if not len(self.file_to_request) == 0:
                    for files_unrequested in self.file_to_request:
                        self.current_unrequested_queue.enqueue(files_unrequested)

                # Init notification service
                notification_receive_thread = Thread(target=self.receive_notification, args=(client_socket,),
                                                     name="NotificationSend")
                notification_receive_thread.start()

                notification_send_thread = Thread(target=self.send_notification,
                                                  args=(client_socket,), name="NotificationReceive")
                notification_send_thread.start()
                notification_receive_thread.join()

                if not self.is_sub_thread_alive.is_sub_thread_alive:
                    client_socket.close()
                    print(f"[Notification Client] Connection with {self.target_ip} closed, doing cleaning job...")
                    self.is_running = False
                    self.restore_status_handler()


        except Exception as e:
            print(f'[Notification Client] Init client connection error {e.__str__()}, switching to server mode...')
            self.is_running = False
