import os
import threading
import json
import utils



class FileInfo:
    def __init__(self, file_path: str, last_modified_time: int, file_md5: str, file_size: int,
                 is_from_remote: bool = False):
        self.file_path = file_path
        self.last_modified_time = last_modified_time
        self.file_md5 = file_md5
        self.file_size = file_size
        self.is_from_remote = is_from_remote

    def __eq__(self, other):
        return self.file_path == other.file_path and self.last_modified_time == other.last_modified_time and self.file_md5 == other.file_md5 and self.file_size == other.file_size

    def __hash__(self):
        return hash((self.file_path, self.last_modified_time))

    def __copy__(self):
        return FileInfo(self.file_path, self.last_modified_time, self.file_md5, self.file_size, self.is_from_remote)


class FileList:
    def __init__(self, file_list_dict: dict = None):
        if file_list_dict is None:
            self.file_list_dict: dict = {}
        else:
            self.file_list_dict = file_list_dict
        self.file_list_lock = threading.Lock()

    def add_file_to_file_list(self, file_info: FileInfo):
        with self.file_list_lock:
            self.file_list_dict[file_info.file_path] = file_info

    def is_file_from_remote(self, file_path: str) -> bool:
        with self.file_list_lock:
            return self.file_list_dict[file_path].is_from_remote

    def get_file_md5(self, file_path: str) -> str:
        with self.file_list_lock:
            return self.file_list_dict[file_path].file_md5

    def get_file_modified_time(self, file_path: str) -> int:
        with self.file_list_lock:
            return self.file_list_dict[file_path].last_modified_time

    def change_file_info_md5(self, file_path: str, new_file_md5: str):
        with self.file_list_lock:
            self.file_list_dict[file_path].file_md5 = new_file_md5

    def change_file_info_size(self, file_path: str, new_file_size: int):
        with self.file_list_lock:
            self.file_list_dict[file_path].file_size = new_file_size

    def change_file_info_mtime(self, file_path: str, new_file_mtime: int):
        with self.file_list_lock:
            self.file_list_dict[file_path].last_modified_time = new_file_mtime

    def change_file_remote_status(self, file_path: str, is_from_remote: bool):
        with self.file_list_lock:
            self.file_list_dict[file_path].is_from_remote = is_from_remote

    def get_file_list_dict(self) -> dict:
        return self.file_list_dict

    def merge_file_list(self, file_list):
        with self.file_list_lock:
            self.file_list_dict.update(file_list.get_file_list_dict())

    def __contains__(self, item):
        return item in self.file_list_dict

    def __getitem__(self, item):
        return self.file_list_dict[item]

    def __setitem__(self, key, value):
        with self.file_list_lock:
            self.file_list_dict[key] = value

    def __iter__(self):
        return self.file_list_dict.__iter__()

    def __delitem__(self, key):
        del self.file_list_dict[key]

    def __str__(self):
        return str(self.file_list_dict)

    def __len__(self):
        with self.file_list_lock:
            return len(self.file_list_dict)

    def __copy__(self):
        copied_file_list_dict = {}
        with self.file_list_lock:
            for path in self:
                copied_file_list_dict[path] = self[path].__copy__()
        return FileList(copied_file_list_dict)

    def init_file_list(self, scanned_folder_path: str):
        self.__init_file_list(scanned_folder_path)
        return self.__copy__()

    def __init_file_list(self, scanned_folder_path: str):
        for item in os.scandir(scanned_folder_path):
            if item.name.startswith('.'):
                continue
            # Deal with locked file
            elif not os.access(item.path, os.R_OK):
                continue
            elif item.is_dir():
                self.file_list_dict.update(self.__init_file_list(item.path))
            else:
                if item.path not in self.file_list_dict:
                    self.file_list_dict[item.path] = (FileInfo(item.path, item.stat().st_mtime_ns,
                                                               utils.get_file_md5(item.path), item.stat().st_size))
                    # print(f"+ {item.name}")
        return self.file_list_dict


def file_list_serialize(file_list: FileList) -> bytes:
    file_list_dict = {}
    for items in file_list:
        file_list_dict[items] = file_list[items].__dict__
    json_byte = json.dumps(file_list_dict).encode('utf-8')
    return json_byte


def file_list_deserialize(json_byte: bytes) -> FileList:
    json_unpacked = json.loads(json_byte.decode('utf-8'))
    for keys in json_unpacked:
        json_unpacked[keys] = FileInfo(**json_unpacked[keys])
    return FileList(json_unpacked)


def get_file_list_diff(this_file_list: FileList, other_file_list: FileList):
    """
    Get the difference between two file lists.
    :param this_file_list: Local file list
    :param other_file_list: Remote file list
    :return: A list of file to request from remote. And a list of file to send to remote.
    """
    file_to_request = FileList()
    file_to_send = FileList()
    for file_name in other_file_list:
        if file_name not in this_file_list:
            file_to_request[file_name] = other_file_list[file_name]
        elif this_file_list[file_name].is_from_remote:
            file_to_request[file_name] = this_file_list[file_name]
        elif this_file_list[file_name].file_md5 != other_file_list[file_name].file_md5:
            if other_file_list[file_name].last_modified_time > this_file_list[file_name].last_modified_time:
                file_to_request[file_name] = other_file_list[file_name]

    for file_name in this_file_list:
        if file_name not in other_file_list:
            file_to_send[file_name] = this_file_list[file_name]
        elif other_file_list[file_name].file_md5 != this_file_list[file_name].file_md5:
            if this_file_list[file_name].last_modified_time > other_file_list[file_name].last_modified_time:
                file_to_send[file_name] = this_file_list[file_name]

    return file_to_request, file_to_send
