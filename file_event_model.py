class FileEvent:
    def __init__(self, event_type: bytes, file_path: str):
        self.event_type = event_type
        self.file_path = file_path


class FileEventWithSize(FileEvent):
    def __init__(self, event_type: bytes, file_path: str, file_size: int):
        super().__init__(event_type, file_path)
        self.file_size = file_size
