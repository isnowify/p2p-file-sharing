import os
import threading
from socket import *
from threading import Thread
from global_variable import *
from file_info_model import FileList, FileInfo
from exception_manager import *
from concurrent_queue import ConcurrentQueue


class TransferServer():
    def __init__(self, target_ip, file_list: FileList):
        self.target_ip = target_ip
        self.server_socket_handler = None
        self.is_running = False
        self.buffer_size = 10485760
        self.clients_connected_socket = {}
        self.is_file_list_diff_finished = False
        self.file_list = file_list

    def init_server_listen(self):
        print(f"[Transfer Server] {self.target_ip}: Start Listening on Port {TRANSFER_PORT}")
        self.server_socket_handler = socket(AF_INET, SOCK_STREAM)
        self.server_socket_handler.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        self.server_socket_handler.bind(("", TRANSFER_PORT))
        self.server_socket_handler.listen(50)
        self.is_running = True
        while True:
            server_socket, address = self.server_socket_handler.accept()
            threading.Thread(target=self.__handle_client, args=(server_socket, address)).start()
            self.clients_connected_socket[address] = server_socket

    def __handle_client(self, server_socket: socket, address):
        # print(f"[Transfer Server] {self.target_ip}: Connected to {address}")

        # Initial Handshake with client
        handshake_success_flag = False
        pre_handshake_code = server_socket.recv(1)
        if len(pre_handshake_code) == 0:
            raise FileSharingException('Handshake Error')
        elif pre_handshake_code == OP_HANDSHAKE:
            # print(f"[Transfer Server] {address}: Handshake with {address} success")
            handshake_success_flag = True
            server_socket.sendall(OP_HANDSHAKE)

        if not handshake_success_flag:
            print(f"[Transfer Server] Handshake with client {address} error, will close this session.")
            server_socket.close()
            return
        else:
            # print(f"[Transfer Server] {address} Start to receive file header...")
            received_file_size = 0
            opcode = server_socket.recv(1)
            if opcode == OP_SEND_FILE:
                file_size = int.from_bytes(server_socket.recv(4), byteorder='big')

                file_path_length = int.from_bytes(server_socket.recv(4), byteorder='big')
                file_path = server_socket.recv(file_path_length).decode('utf-8')

                md5_length = int.from_bytes(server_socket.recv(4), byteorder='big')
                file_md5 = server_socket.recv(md5_length).decode('utf-8')

                self.file_list.add_file_to_file_list(
                    FileInfo(file_path, 0, file_md5, file_size, True))
                # print(f"[Transfer Server] : Receive file {file_path} of {file_size} from {address}")

                # Check if folder exists, if not, create one
                os.makedirs(os.path.dirname(file_path), exist_ok=True)
                with open(file_path, 'wb') as f:
                    while received_file_size < file_size:
                        if file_size - received_file_size > self.buffer_size:
                            _buffer_size = self.buffer_size
                        else:
                            _buffer_size = file_size - received_file_size
                        data = server_socket.recv(_buffer_size)
                        f.write(data)
                        received_file_size += len(data)
                self.file_list.change_file_info_mtime(file_path, os.stat(file_path).st_mtime_ns)
                self.file_list.change_file_remote_status(file_path, False)
                # print(f"[Transfer Server] {address}: Downloaded {file_path}")
                server_socket.close()


class TransferClient():
    def __init__(self, target_ip: str, file_list: FileList):
        self.target_ip = target_ip
        self.is_running = False
        self.file_list = file_list
        self.transfer_buffer_size = 10485760

    def init_file_sending(self, file_path: str, thread_id: int):
        # print(f"[Transfer Client - {thread_id}] Start connecting to {self.target_ip}")
        client_socket = socket(AF_INET, SOCK_STREAM)
        client_socket.connect((self.target_ip, TRANSFER_PORT))
        # print(
        #     f"[Transfer Client - {thread_id}] Successfully connected to {self.target_ip} {client_socket.getsockname()}")
        self.is_running = True

        # Initial Handshake with client
        handshake_success_flag = False
        client_socket.sendall(OP_HANDSHAKE)
        pre_handshake_code = client_socket.recv(1)

        if pre_handshake_code == OP_HANDSHAKE:
            print(f"[Transfer Client - {thread_id}] {self.target_ip}: Handshake with {self.target_ip} success")
            handshake_success_flag = True

        if not handshake_success_flag:
            print(f"[Transfer Client - {thread_id}] Handshake with client failed, will close this session.")
            client_socket.close()
        else:
            # Start send file to remote
            try:
                # print(f"[Transfer Client - {thread_id}] Start to send file info {file_path}")
                client_socket.sendall(OP_SEND_FILE)
                file_size = os.path.getsize(file_path)
                client_socket.sendall(file_size.to_bytes(4, byteorder='big'))

                file_path_bytes = file_path.encode('utf-8')
                client_socket.sendall(len(file_path_bytes).to_bytes(4, byteorder='big'))
                client_socket.sendall(file_path_bytes)

                file_md5_byte = self.file_list.get_file_md5(file_path).encode('utf-8')
                client_socket.sendall(len(file_md5_byte).to_bytes(4, byteorder='big'))
                client_socket.sendall(file_md5_byte)

                # print(f"[Transfer Client - {thread_id}] Start to send file {file_path}")

                bytes_sent = 0

                with open(file_path, 'rb') as f:
                    while bytes_sent < file_size:
                        if file_size - bytes_sent < self.transfer_buffer_size:
                            __buffer_size = file_size - bytes_sent
                        else:
                            __buffer_size = self.transfer_buffer_size
                        data = f.read(__buffer_size)
                        client_socket.sendall(data)
                        bytes_sent += len(data)

                print(f"[Transfer Client - {thread_id}] File {file_path} sent.")
                client_socket.close()
            except Exception as e:
                print(f"[Transfer Client - {thread_id}] {file_path} Disconnected {e}, closing...")
                client_socket.close()


class TransferClientPool():
    def __init__(self, transfer_client: TransferClient):
        self.transfer_client = transfer_client
        self.task_path_pool = ConcurrentQueue()
        self.allowed_thread_num = 10
        self.current_thread_running_lock = threading.Lock()
        self.current_thread_running_id = 0
        self.on_task_finished_event = threading.Event()

    def clear_task_pool(self):
        with self.current_thread_running_lock:
            self.task_path_pool.clear()
            self.current_thread_running_id = 0
            self.on_task_finished_event.set()

    def submit(self, file_path: str):
        with self.current_thread_running_lock:
            if self.current_thread_running_id < self.allowed_thread_num:
                download_thread = Thread(target=self.__start_thread, args=(file_path,),
                                         name=f"Download Thread - {self.current_thread_running_id}")
                self.current_thread_running_id += 1
                download_thread.start()
            else:
                self.task_path_pool.enqueue(file_path)

    def __start_thread(self, file_path: str):
        # print(f"[Transfer Client Pool] thread - {self.current_thread_running_id} start working")
        self.transfer_client.init_file_sending(file_path=file_path, thread_id=self.current_thread_running_id)
        # print(f"[Transfer Client Pool] thread - {self.current_thread_running_id} finished")

        while True:
            next_file_path = self.task_path_pool.dequeue()
            if next_file_path is not None:
                # print(f"[Transfer Client Pool] thread - {self.current_thread_running_id} continues working")
                self.transfer_client.init_file_sending(next_file_path, self.current_thread_running_id)
                # print(f"[Transfer Client Pool] thread - {self.current_thread_running_id} finished")
            else:
                break

        # Cleanup after all tasks finished

        with self.current_thread_running_lock:
            self.current_thread_running_id -= 1
        if self.current_thread_running_lock == 0:
            self.on_task_finished_event.set()
        # print(f"[Transfer Client Pool] thread - {self.current_thread_running_id} finished")
