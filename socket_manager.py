from notification_manager import *
from transfer_manager import *
from concurrent_queue import ConcurrentQueue
from event_handler import NotifyEventHandler


class SocketManager:
    def __init__(self, target_ip, initial_file_list, current_local_file_event_queue: ConcurrentQueue,
                 file_list: FileList):
        self.target_ip = target_ip
        self.initial_file_list = initial_file_list
        self.file_to_request = {}
        self.file_to_send = {}
        self.file_list = file_list
        self.current_local_file_event_queue = current_local_file_event_queue
        self.current_unrequested_queue = ConcurrentQueue()
        self.notify_event_handler = NotifyEventHandler(self.target_ip, self.current_local_file_event_queue,
                                                       self.current_unrequested_queue, self.file_list)

        self.notify_server = NotifyServer(self.target_ip, self.initial_file_list, self.file_to_request,
                                          self.file_to_send, self.current_unrequested_queue, self.notify_event_handler)
        self.notify_client = NotifyClient(self.target_ip, self.initial_file_list, self.file_to_request
                                          , self.current_unrequested_queue, self.notify_event_handler,
                                          self.restore_status)

        self.transfer_server = TransferServer(target_ip, self.file_list)

    def run_notification_client(self):
        self.notify_client.init_client_connection()

    def run_notification_server(self):
        self.notify_server.init_server_listen()

    def run_transfer_server(self):
        self.transfer_server.init_server_listen()

    def restore_status(self):
        self.current_local_file_event_queue.clear()
        self.initial_file_list = self.file_list.__copy__()
        self.notify_server.restore_status(self.initial_file_list)
        self.current_unrequested_queue.clear()
