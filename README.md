# P2P-File-Sharing

## Description

A P2P file transmission utility that automatically synchronizes files among two devices.

## Usage

Clone and simply run `python main.py --ip <target IP> ` and put files you want to sync in `./share` .

## TODO

- [x] Multi-threading

- [x] File monitor upon file changes

- [x] File diff check

- [x] Reconnect upon connection failure

- [x] File hash check

- [x] Partial transmission

- [ ] Client auto detection

- [ ] Support for more than 2 devices

## License

This project is released under the MIT license. See [LICENSE](https://gitlab.com/isnowify/p2p-file-sharing/-/blob/main/LICENSE) for more details.
