# Constants
NOTIFY_PORT = 22500
TRANSFER_PORT = 22501

# Operation Codes
OP_HANDSHAKE = b'\x01'
OP_SEND_FILE_LIST = b'\x02'
OP_SEND_FILE_LIST_DIFF = b'\x03'
OP_REQUEST_FILE = b'\x04'
OP_SEND_FILE = b'\x05'

OP_NOTIFY_FILE_ADDED = b'\x06'
OP_NOTIFY_FILE_CHANGED = b'\x07'
