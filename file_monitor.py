import os
import time
from global_variable import *
from concurrent_queue import ConcurrentQueue
from file_info_model import FileInfo, FileList
from file_event_model import FileEvent
import utils


class FileMonitor:
    def __init__(self, target_folder_path: str, current_local_file_event_queue: ConcurrentQueue, file_list: FileList):
        self.target_folder_path = target_folder_path
        self.file_list = file_list
        self.current_local_file_event_queue = current_local_file_event_queue

    def start_monitoring(self):
        while True:
            self.traversal_folder(self.target_folder_path)
            time.sleep(0.02)

    def traversal_folder(self, scanned_folder_path: str):

        for item in os.scandir(scanned_folder_path):
            if item.name.startswith('.'):
                continue
            elif not os.access(item.path, os.R_OK):
                continue
            if item.is_dir():
                self.file_list.merge_file_list(self.traversal_folder(item.path))
            else:
                if item.path not in self.file_list:
                    self.file_list.add_file_to_file_list(FileInfo(item.path, item.stat().st_mtime_ns,
                                                                  utils.get_file_md5(item.path), item.stat().st_size))
                    # on added new file
                    added_file_event = FileEvent(OP_NOTIFY_FILE_ADDED, item.path)
                    self.current_local_file_event_queue.enqueue(added_file_event)
                    # print(f"+ {item.name}")
                else:
                    if self.file_list.is_file_from_remote(item.path):
                        continue
                    elif self.file_list.get_file_modified_time(item.path) != item.stat().st_mtime_ns:
                        current_md5 = utils.get_file_md5(item.path)
                        if current_md5 != self.file_list.get_file_md5(item.path):
                            self.file_list.change_file_info_md5(item.path, current_md5)
                            self.file_list.change_file_info_size(item.path, item.stat().st_size)
                            # on modified file
                            modified_file_event = FileEvent(OP_NOTIFY_FILE_CHANGED, item.path)
                            self.current_local_file_event_queue.enqueue(modified_file_event)
                            # print(f"M {item.name}")
                        self.file_list.change_file_info_mtime(item.path, item.stat().st_mtime_ns)
        return self.file_list
